<?php

/**
 * @file
 * Contains SjiSocialConnectException.
 */

namespace Drupal\sjisocialconnect;

/**
 * Exception to use when no definitions are found for a widget.
 */
class SjiSocialConnectException extends \Exception { }
