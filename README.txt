Social  Media Integration
--------------------------

Maintainers:
	Sjinnovation - https://www.drupal.org/u/sjinnovation
	Requires - Drupal 8

Overview:
-------------------------

Social  Media Integration is a light-weight, customizable plugin.
It allows integration of social media feeds into your website.
Easily Configurable.



